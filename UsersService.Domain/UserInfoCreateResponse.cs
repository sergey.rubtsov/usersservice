using System.Xml.Serialization;

namespace UsersService.Domain
{
    public class UserInfoCreateResponse : Response
    {
        public UserInfoCreateResponse()
        {
        }

        public UserInfoCreateResponse(UserInfo userInfo)
        {
            ErrorId = ErrorId.None;
            Success = true;
            UserInfo = userInfo;
        }

        [XmlElement("user")]
        public UserInfo UserInfo { get; set; }
    }
}
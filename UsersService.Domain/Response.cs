using System.Text.Json.Serialization;
using System.Xml.Serialization;

namespace UsersService.Domain
{
    [XmlInclude(typeof(FailResponse))]
    [XmlInclude(typeof(UserInfoCreateResponse))]
    public abstract class Response
    {
        [JsonPropertyName(nameof(ErrorId))]
        [XmlIgnore]
        public ErrorId ErrorId { get; set; }

        [JsonIgnore]
        [XmlAttribute(nameof(ErrorId))]
        public int ErrorIdAsInt
        {
            get => (int) ErrorId;
            set => ErrorId = (ErrorId) value;
        }

        [JsonPropertyName(nameof(Success))]
        [XmlAttribute]
        public bool Success { get; set; }
    }
}
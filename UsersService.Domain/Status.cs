namespace UsersService.Domain
{
    public enum Status
    {
        New = 0,
        Active = 10,
        Blocked = 20,
        Deleted = 30,
    }
}
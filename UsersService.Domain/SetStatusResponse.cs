using System.Text.Json.Serialization;

namespace UsersService.Domain
{
    public class SetStatusResponse : Response
    {
        public SetStatusResponse(UserInfo userInfo)
        {
            UserId = userInfo.Id;
            Status = userInfo.Status;
            Name = userInfo.Name;
        }

        [JsonPropertyName("Id")]
        public int UserId { get; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        [JsonPropertyName(nameof(Status))]
        public Status Status { get; }

        [JsonPropertyName(nameof(Name))]
        public string Name { get; }
    }
}
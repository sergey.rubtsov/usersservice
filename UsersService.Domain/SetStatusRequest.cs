namespace UsersService.Domain
{
    public class SetStatusRequest
    {
        public int Id { get; set; }
        public string NewStatus { get; set; }
    }
}
using System.Text.Json.Serialization;
using System.Xml.Serialization;

namespace UsersService.Domain
{
    public class FailResponse : Response
    {
        public FailResponse()
        {
        }

        public FailResponse(ErrorId errorId, string message)
        {
            ErrorId = errorId;
            Success = false;
            Message = message;
        }

        [JsonPropertyName("Msg")]
        [XmlElement("ErrorMsg")]
        public string Message { get; set; }
    }
}
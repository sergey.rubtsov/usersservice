namespace UsersService.Domain
{
    public class UserInfoRemoveRequest
    {
        public UserInfo RemoveUser { get; set; }
    }
}
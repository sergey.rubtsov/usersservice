using System.Xml.Serialization;

namespace UsersService.Domain
{
    [XmlRoot("Request")]
    public class UserInfoCreateRequest
    {
        [XmlElement("user")]
        public UserInfo UserInfo { get; set; }
    }
}
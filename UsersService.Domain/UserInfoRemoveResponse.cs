using System.Text.Json.Serialization;

namespace UsersService.Domain
{
    public class UserInfoRemoveResponse : Response
    {
        public UserInfoRemoveResponse(UserInfo userInfo)
        {
            UserInfo = userInfo;
            Message = "User was removed";
            Success = true;
        }

        [JsonPropertyName("user")]
        public UserInfo UserInfo { get; }

        [JsonPropertyName("Msg")]
        public string Message { get; }
    }
}
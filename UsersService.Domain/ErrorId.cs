namespace UsersService.Domain
{
    public enum ErrorId
    {
        None = 0,
        UserAlreadyExists = 1,
        NotFound = 2,
        Unknown = 99
    }
}
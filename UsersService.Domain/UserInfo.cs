using System.Text.Json.Serialization;
using System.Xml.Serialization;

namespace UsersService.Domain
{
    [XmlRoot("user")]
    public class UserInfo
    {
        [JsonPropertyName(nameof(Id))]
        [XmlAttribute]
        public int Id { get; set; }

        [JsonPropertyName(nameof(Name))]
        [XmlAttribute]
        public string Name { get; set; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        [JsonPropertyName(nameof(Status))]
        public Status Status { get; set; }
    }
}
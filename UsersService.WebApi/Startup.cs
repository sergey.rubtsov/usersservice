using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using UsersService.WebApi.Models;
using UsersService.WebApi.Options;
using UsersService.WebApi.Services;

namespace UsersService.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CacheOptions>(Configuration.GetSection("Cache"));
            services.Configure<CredentialsOptions>(Configuration.GetSection("Credentials"));

            services.AddDbContext<IUsersServiceContext, UsersServiceContext>(
                optionsBuilder => optionsBuilder
                    .UseMySql(Configuration.GetConnectionString("UsersServiceDatabase")));

            services.AddSingleton<IUserInfoCache, UserInfoCache>();

            services.AddScoped<IUserInfoRepository, UserInfoRepository>();

            services.AddScoped<IUserAuthenticationService, UserAuthenticationService>();

            services.AddAuthentication("BasicAuthentication")
                .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("BasicAuthentication", null);

            services.AddControllersWithViews()
                .AddXmlSerializerFormatters();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(
                endpoints =>
                {
                    endpoints.MapControllers();
                });

            EnsureDatabaseCreated(app);
        }

        private void EnsureDatabaseCreated(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.CreateScope();
            var usersServiceContext = serviceScope.ServiceProvider.GetService<IUsersServiceContext>();
            usersServiceContext.Database.EnsureCreated();
        }
    }
}
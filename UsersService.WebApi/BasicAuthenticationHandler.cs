using System;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using UsersService.WebApi.Services;

namespace UsersService.WebApi
{
    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly IUserAuthenticationService _userAuthenticationService;

        public BasicAuthenticationHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            IUserAuthenticationService userAuthenticationService)
            : base(
                options,
                logger,
                encoder,
                clock)
        {
            _userAuthenticationService = userAuthenticationService;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (IsAnonymous())
                return AuthenticateResult.NoResult();

            if (IsMissedAuthorizationHeader())
                return AuthenticateResult.Fail("Missed Authorization header");

            var (username, password) = GetCredentials();

            if (!await IsAuthenticated(username, password))
                return AuthenticateResult.Fail("Invalid username or password");

            var successResult = ConstructSuccessResult(username);
            return successResult;
        }

        private bool IsAnonymous()
        {
            var endpoint = Context.GetEndpoint();
            var isAnonymous = endpoint?.Metadata?.GetMetadata<IAllowAnonymous>() != null;
            return isAnonymous;
        }

        private bool IsMissedAuthorizationHeader()
        {
            return !Request.Headers.ContainsKey("Authorization");
        }

        private (string username, string password) GetCredentials()
        {
            var authenticationHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
            var credentialBytes = Convert.FromBase64String(authenticationHeader.Parameter);
            var credentials = Encoding.UTF8.GetString(credentialBytes).Split(':');
            var username = credentials[0];
            var password = credentials[1];
            return (username, password);
        }

        private async Task<bool> IsAuthenticated(string username, string password)
        {
            return await _userAuthenticationService.Verify(username, password);
        }

        private AuthenticateResult ConstructSuccessResult(string username)
        {
            var claims = new[] { new Claim(ClaimTypes.Name, username) };
            var claimsIdentity = new ClaimsIdentity(claims, Scheme.Name);
            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
            var authenticationTicket = new AuthenticationTicket(claimsPrincipal, Scheme.Name);
            var authenticateResult = AuthenticateResult.Success(authenticationTicket);
            return authenticateResult;
        }
    }
}
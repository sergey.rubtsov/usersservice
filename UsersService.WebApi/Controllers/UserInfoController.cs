using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UsersService.Domain;
using UsersService.WebApi.Exceptions;
using UsersService.WebApi.Services;

namespace UsersService.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("~")]
    public class UserInfoController : Controller
    {
        private readonly IUserInfoRepository _userInfoRepository;

        public UserInfoController(IUserInfoRepository userInfoRepository)
        {
            _userInfoRepository = userInfoRepository;
        }

        [HttpPost("Auth/[action]")]
        [Produces("application/xml")]
        public async Task<Response> CreateUser(UserInfoCreateRequest request)
        {
            async Task<Response> InnerCreateUser()
            {
                var userInfo = request.UserInfo;
                await _userInfoRepository.AddNew(userInfo);
                return new UserInfoCreateResponse(userInfo);
            }

            return await SafeExecute(InnerCreateUser);
        }

        [HttpPost("Auth/[action]")]
        [Produces("application/json")]
        public async Task<Response> RemoveUser(UserInfoRemoveRequest request)
        {
            async Task<Response> InnerRemoveUser()
            {
                var userInfo = await _userInfoRepository.Remove(request.RemoveUser.Id);
                return new UserInfoRemoveResponse(userInfo);
            }

            return await SafeExecute(InnerRemoveUser);
        }

        [HttpPost("Auth/[action]")]
        [Produces("application/json")]
        public async Task<Response> SetStatus([FromForm] SetStatusRequest request)
        {
            async Task<Response> InnerSetStatus()
            {
                var newStatus = Enum.Parse<Status>(request.NewStatus);
                var userInfo = await _userInfoRepository.SetStatus(request.Id, newStatus);
                return new SetStatusResponse(userInfo);
            }

            return await SafeExecute(InnerSetStatus);
        }

        [AllowAnonymous]
        [HttpGet("Public/UserInfo")]
        [Produces("text/html")]
        public async Task<IActionResult> GetUserInfo(int id)
        {
            try
            {
                var userInfo = await _userInfoRepository.Get(id);
                return View("~/Views/UserInfo.cshtml", userInfo);
            }
            catch (UserNotFoundException)
            {
                return NotFound();
            }
        }

        private async Task<Response> SafeExecute(Func<Task<Response>> func)
        {
            try
            {
                return await func();
            }
            catch (UsersServiceException usersServiceException)
            {
                return new FailResponse(usersServiceException.ErrorId, usersServiceException.Message);
            }
            catch (Exception exception)
            {
                return new FailResponse(ErrorId.Unknown, exception.Message);
            }
        }
    }
}
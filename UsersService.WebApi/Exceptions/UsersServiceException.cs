using System;
using UsersService.Domain;
using UsersService.WebApi.Models;

namespace UsersService.WebApi.Exceptions
{
    public class UsersServiceException : Exception
    {
        public UsersServiceException(ErrorId errorId, string message) : base(message)
        {
            ErrorId = errorId;
        }

        public ErrorId ErrorId { get; }
    }
}
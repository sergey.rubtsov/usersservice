using UsersService.Domain;

namespace UsersService.WebApi.Exceptions
{
    public class UserNotFoundException : UsersServiceException
    {
        public UserNotFoundException(string message)
            : base(ErrorId.NotFound, message)
        {
        }
    }
}
using UsersService.Domain;

namespace UsersService.WebApi.Exceptions
{
    public class UserAlreadyExistsException : UsersServiceException
    {
        public UserAlreadyExistsException(string message) : base(ErrorId.UserAlreadyExists, message)
        {
        }
    }
}
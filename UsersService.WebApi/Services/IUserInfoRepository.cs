using System.Threading.Tasks;
using UsersService.Domain;
using UsersService.WebApi.Models;

namespace UsersService.WebApi.Services
{
    public interface IUserInfoRepository
    {
        Task AddNew(UserInfo userInfo);

        Task<UserInfo> Get(int id);

        Task<UserInfo> Remove(int id);

        Task<UserInfo> SetStatus(int id, Status newStatus);
    }
}
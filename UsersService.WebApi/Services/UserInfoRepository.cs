using System.Threading.Tasks;
using UsersService.Domain;
using UsersService.WebApi.Exceptions;
using UsersService.WebApi.Models;

namespace UsersService.WebApi.Services
{
    public class UserInfoRepository : IUserInfoRepository
    {
        private readonly IUsersServiceContext _usersServiceContext;
        private readonly IUserInfoCache _userInfoCache;

        public UserInfoRepository(IUsersServiceContext usersServiceContext, IUserInfoCache userInfoCache)
        {
            _usersServiceContext = usersServiceContext;
            _userInfoCache = userInfoCache;
        }

        public async Task AddNew(UserInfo userInfo)
        {
            var isExist = _userInfoCache.IsExist(userInfo.Id);
            if (isExist)
                throw new UserAlreadyExistsException($"User with id {userInfo.Id} already exist");

            await _usersServiceContext.AddAsync(userInfo);
            await _usersServiceContext.SaveChangesAsync();
            _userInfoCache.AddNew(userInfo);
        }

        public Task<UserInfo> Get(int id)
        {
            var userInfo = _userInfoCache.Get(id);
            if (userInfo == null)
                throw new UserNotFoundException("User not found");

            return Task.FromResult(userInfo);
        }

        public async Task<UserInfo> Remove(int id)
        {
            var userInfo = await Get(id);

            await UpdateStatus(userInfo, Status.Deleted);
            return userInfo;
        }

        public async Task<UserInfo> SetStatus(int id, Status newStatus)
        {
            var userInfo = await Get(id);

            await UpdateStatus(userInfo, newStatus);
            return userInfo;
        }

        private async Task UpdateStatus(UserInfo userInfo, Status newStatus)
        {
            userInfo.Status = newStatus;
            _usersServiceContext.Update(userInfo);
            await _usersServiceContext.SaveChangesAsync();
        }
    }
}
using System.Threading.Tasks;

namespace UsersService.WebApi.Services
{
    public interface IUserAuthenticationService
    {
        Task<bool> Verify(string username, string password);
    }
}
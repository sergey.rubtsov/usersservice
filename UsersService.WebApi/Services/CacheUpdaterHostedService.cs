using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using UsersService.Domain;
using UsersService.WebApi.Models;
using UsersService.WebApi.Options;

namespace UsersService.WebApi.Services
{
    public class CacheUpdaterHostedService : IHostedService, IDisposable
    {
        private readonly ILogger<CacheUpdaterHostedService> _logger;
        private readonly IServiceProvider _serviceProvider;
        private readonly IUserInfoCache _userInfoCache;
        private Timer _updateCacheTimer;
        private readonly TimeSpan _updateCachePeriod;

        public CacheUpdaterHostedService(
            ILogger<CacheUpdaterHostedService> logger,
            IServiceProvider serviceProvider,
            IUserInfoCache userInfoCache,
            IOptions<CacheOptions> cacheOptions)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
            _userInfoCache = userInfoCache;
            _updateCachePeriod = TimeSpan.FromMinutes(cacheOptions.Value.UpdatePeriodInMinutes);
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("StartAsync");
            
            _updateCacheTimer = new Timer(UpdateCache, null, TimeSpan.Zero, _updateCachePeriod);

            return Task.CompletedTask;
        }

        private void UpdateCache(object state)
        {
            _logger.LogInformation("UpdateCache");

            var userInfos = LoadUserInfos();
            _userInfoCache.Reset(userInfos);

            _logger.LogInformation($"Loaded {userInfos.Count} users");
        }

        private List<UserInfo> LoadUserInfos()
        {
            using var scope = _serviceProvider.CreateScope();
            var usersServiceContext = scope.ServiceProvider.GetService<IUsersServiceContext>();
            var userInfos = usersServiceContext.UserInfos.ToList();
            return userInfos;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("StopAsync");

            _updateCacheTimer?.Change(Timeout.Infinite, 0);
            
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _logger.LogInformation("Dispose");
            _updateCacheTimer?.Dispose();
        }
    }
}
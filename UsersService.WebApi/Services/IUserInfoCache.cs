using System.Collections.Generic;
using UsersService.Domain;

namespace UsersService.WebApi.Services
{
    public interface IUserInfoCache
    {
        void AddNew(UserInfo userInfo);

        void Reset(IEnumerable<UserInfo> userInfos);

        bool IsExist(int id);

        UserInfo Get(int id);
    }
}
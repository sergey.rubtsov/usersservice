using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using UsersService.WebApi.Options;

namespace UsersService.WebApi.Services
{
    internal class UserAuthenticationService : IUserAuthenticationService
    {
        private readonly CredentialsOptions _credentialsOptions;

        public UserAuthenticationService(IOptions<CredentialsOptions> credentialsOptions)
        {
            _credentialsOptions = credentialsOptions.Value;
        }

        public Task<bool> Verify(string username, string password)
        {
            var isVerified = username == _credentialsOptions.Username &&
                             password == _credentialsOptions.Password;
            return Task.FromResult(isVerified);
        }
    }
}
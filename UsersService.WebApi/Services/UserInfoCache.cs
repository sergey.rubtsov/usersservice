using System.Collections.Generic;
using System.Linq;
using UsersService.Domain;

namespace UsersService.WebApi.Services
{
    public class UserInfoCache : IUserInfoCache
    {
        private static readonly object _syncObject = new object();
        private readonly List<UserInfo> _innerUserInfos;

        public UserInfoCache()
        {
            _innerUserInfos = new List<UserInfo>();
        }

        public void AddNew(UserInfo userInfo)
        {
            lock (_syncObject)
            {
                _innerUserInfos.Add(userInfo);
            }
        }

        public void Reset(IEnumerable<UserInfo> newUserInfos)
        {
            lock (_syncObject)
            {
                _innerUserInfos.Clear();
                _innerUserInfos.AddRange(newUserInfos);
            }
        }

        public bool IsExist(int id)
        {
            lock (_syncObject)
            {
                return _innerUserInfos.Any(userInfo => userInfo.Id == id);
            }
        }

        public UserInfo Get(int id)
        {
            lock (_syncObject)
            {
                return _innerUserInfos.SingleOrDefault(userInfo => userInfo.Id == id);
            }
        }
    }
}
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using UsersService.Domain;

namespace UsersService.WebApi.Models
{
    public interface IUsersServiceContext
    {
        DbSet<UserInfo> UserInfos { get; }

        DatabaseFacade Database { get; }

        ValueTask<EntityEntry<TEntity>> AddAsync<TEntity>(TEntity entity, CancellationToken cancellationToken = default)
            where TEntity : class;

        EntityEntry Update(object entity);

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
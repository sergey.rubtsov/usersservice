using Microsoft.EntityFrameworkCore;
using UsersService.Domain;

namespace UsersService.WebApi.Models
{
    public class UsersServiceContext : DbContext, IUsersServiceContext
    {
        public UsersServiceContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<UserInfo> UserInfos { get; set; }
    }
}
namespace UsersService.WebApi.Options
{
    public class CacheOptions
    {
        public int UpdatePeriodInMinutes { get; set; }
    }
}
namespace UsersService.WebApi.Options
{
    public class CredentialsOptions
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
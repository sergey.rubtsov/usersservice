using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace UsersService.ConsoleApp
{
    public class HttpClientFactory
    {
        private const string BaseAddress = "https://localhost:44329/";
        private const string Username = "user";
        private const string Password = "password";

        public static HttpClient Create(bool useBasicAuthorization)
        {
            var loggingHandler = new LoggingHandler(new HttpClientHandler());
            var baseAddress = new Uri(BaseAddress);
            var authenticationHeaderValue = useBasicAuthorization ? CreateAuthenticationHeader() : null;
            var httpClient = new HttpClient(loggingHandler)
                { BaseAddress = baseAddress, DefaultRequestHeaders = { Authorization = authenticationHeaderValue } };

            return httpClient;
        }

        private static AuthenticationHeaderValue CreateAuthenticationHeader()
        {
            var credential = $"{Username}:{Password}";
            var credentialAsBytes = Encoding.UTF8.GetBytes(credential);
            var credentialAsBase64 = Convert.ToBase64String(credentialAsBytes);
            var authenticationHeaderValue = new AuthenticationHeaderValue("Basic", credentialAsBase64);
            return authenticationHeaderValue;
        }
    }
}
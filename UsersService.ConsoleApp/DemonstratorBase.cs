using System;
using System.Collections.Specialized;
using System.Threading.Tasks;
using System.Web;

namespace UsersService.ConsoleApp
{
    internal abstract class DemonstratorBase
    {
        public abstract string MethodName { get; }

        public async Task Start()
        {
            Console.WriteLine($"Method: {MethodName}");

            await ExecuteCases();
        }

        protected abstract Task ExecuteCases();
    }
}
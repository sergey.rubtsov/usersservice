﻿using System;
using System.Threading.Tasks;

namespace UsersService.ConsoleApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await DemonstrateMethodsWithAuth();

            await DemonstrateMethodsWithoutAuth();

            WaitForUserInput();
        }

        private static async Task DemonstrateMethodsWithAuth()
        {
            using var httpClientWithAuthorization = HttpClientFactory.Create(true);

            var createUserDemonstrator = new CreateUserDemonstrator(httpClientWithAuthorization);
            await createUserDemonstrator.Start();

            var removeUserDemonstrator = new RemoveUserDemonstrator(httpClientWithAuthorization);
            await removeUserDemonstrator.Start();

            var setStatusDemonstrator = new SetStatusDemonstrator(httpClientWithAuthorization);
            await setStatusDemonstrator.Start();
        }

        private static async Task DemonstrateMethodsWithoutAuth()
        {
            using var httpClientWithoutAuthorization = HttpClientFactory.Create(false);

            var getUserInfoDemonstrator = new GetUserInfoDemonstrator(httpClientWithoutAuthorization);
            await getUserInfoDemonstrator.Start();
        }

        private static void WaitForUserInput()
        {
            Console.WriteLine("Press any key to exit");
            Console.Read();
        }
    }
}
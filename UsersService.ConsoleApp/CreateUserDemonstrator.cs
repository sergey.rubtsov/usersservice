using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace UsersService.ConsoleApp
{
    internal class CreateUserDemonstrator : DemonstratorBase
    {
        private readonly HttpClient _httpClient;

        public CreateUserDemonstrator(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public override string MethodName { get; } = "CreateUser";

        protected override async Task ExecuteCases()
        {
            await SendSuccessCreateUser(_httpClient);
            await SendCreateExistUser(_httpClient);
        }

        private async Task SendSuccessCreateUser(HttpClient httpClient)
        {
            Console.WriteLine("Valid request");
            var createUserRequest = 
                @"<Request>
  <user Id=""999"" Name=""alex"">
    <Status>New</Status>
  </user>
</Request>";

            var content = new StringContent(createUserRequest, Encoding.UTF8, "application/xml");
            await _httpClient.PostAsync("/Auth/CreateUser", content);
            Console.WriteLine();
        }

        private async Task SendCreateExistUser(HttpClient httpClient)
        {
            Console.WriteLine("Request with already exists user");
            var createUserRequest = 
                @"<Request>
  <user Id=""999"" Name=""alex"">
    <Status>New</Status>
  </user>
</Request>";

            var content = new StringContent(createUserRequest, Encoding.UTF8, "application/xml");
            await _httpClient.PostAsync("/Auth/CreateUser", content);
            Console.WriteLine();
        }
    }
}
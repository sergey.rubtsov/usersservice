using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;

namespace UsersService.ConsoleApp
{
    public static class CustomHttpClientExtensions
    {
        public static Task<HttpResponseMessage> PostAsXmlAsync<T>(
            this HttpClient httpClient,
            string requestUri,
            T value,
            bool useXmlSerializer = true)
        {
            return httpClient.PostAsync(requestUri, value, new XmlMediaTypeFormatter { UseXmlSerializer = useXmlSerializer, Indent = true });
        }
    }

    public static class CustomHttpContentExtensions
    {
        private static MediaTypeFormatterCollection? _defaultMediaTypeFormatterCollection;

        private static MediaTypeFormatterCollection DefaultMediaTypeFormatterCollection
        {
            get
            {
                if (_defaultMediaTypeFormatterCollection == null)
                    _defaultMediaTypeFormatterCollection =
                        new MediaTypeFormatterCollection(
                            new MediaTypeFormatter[]
                            {
                                new JsonMediaTypeFormatter(),
                                new XmlMediaTypeFormatter { UseXmlSerializer = true, Indent = true }
                            });
                return _defaultMediaTypeFormatterCollection;
            }
        }

        public static Task<T> ReadAsAsync<T>(this HttpContent content) => content.ReadAsAsync<T>(DefaultMediaTypeFormatterCollection);
    }
}
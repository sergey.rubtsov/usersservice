using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace UsersService.ConsoleApp
{
    internal class LoggingHandler : DelegatingHandler
    {
        public LoggingHandler(HttpMessageHandler innerHandler)
            : base(innerHandler)
        {
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            Console.ForegroundColor = ConsoleColor.DarkGray;

            await LogRequest(request);

            var response = await base.SendAsync(request, cancellationToken);

            await LogResponse(response);

            Console.ResetColor();

            return response;
        }

        private static async Task LogRequest(HttpRequestMessage request)
        {
            Console.WriteLine($"Request. {request}");
            if (request.Content != null)
                Console.WriteLine(await request.Content.ReadAsStringAsync());
        }

        private async Task LogResponse(HttpResponseMessage response)
        {
            Console.WriteLine("Response. StatusCode: 200, ReasonPhrase: 'OK', Headers:");
            HeadersAsString(response.Headers, response.Content?.Headers);
            if (response.Content != null)
            {
                var content = await TryGetContentAsString(response);
                Console.WriteLine(content);
            }
        }

        private void HeadersAsString(params HttpHeaders?[] headers)
        {
            Console.WriteLine("{");
            for (var i = 0; i < headers.Length; i++)
            {
                if (headers[i] == null)
                    continue;

                foreach (var header in headers[i]!)
                {
                    if (header.Key != "Content-Type")
                        continue;
                    foreach (var headerValue in header.Value)
                    {
                        Console.WriteLine($"  {header.Key}: {headerValue}");
                    }
                }
            }

            Console.WriteLine("}");
        }

        private static async Task<string> TryGetContentAsString(HttpResponseMessage response)
        {
            var content = await response.Content.ReadAsStringAsync();

            var contentType = response.Content.Headers.ContentType.MediaType;

            if (contentType == "application/xml")
                content = BeautifyXml(content);

            return content;
        }

        private static string BeautifyXml(string xml)
        {
            try
            {
                XDocument doc = XDocument.Parse(xml);
                return doc.ToString();
            }
            catch (Exception)
            {
                return xml;
            }
        }
    }
}
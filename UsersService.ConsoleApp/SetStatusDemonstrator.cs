using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using UsersService.Domain;

namespace UsersService.ConsoleApp
{
    internal class SetStatusDemonstrator : DemonstratorBase
    {
        private readonly HttpClient _httpClient;

        public SetStatusDemonstrator(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public override string MethodName { get; } = "SetStatus";

        protected override async Task ExecuteCases()
        {
            await SetNewStatusForUser();
        }

        private async Task SetNewStatusForUser()
        {
            HttpContent content = new FormUrlEncodedContent(
                new[]
                {
                    new KeyValuePair<string, string>("id", 999.ToString()),
                    new KeyValuePair<string, string>("newStatus", Status.Blocked.ToString())
                });

            await _httpClient.PostAsync($"/Auth/SetStatus", content);
        }
    }
}
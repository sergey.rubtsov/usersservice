using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace UsersService.ConsoleApp
{
    internal class RemoveUserDemonstrator : DemonstratorBase
    {
        private readonly HttpClient _httpClient;

        public RemoveUserDemonstrator(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public override string MethodName { get; } = "RemoveUser";

        protected override async Task ExecuteCases()
        {
            await RemoveExistsUser();
            await RemoveNonExistUser();
        }

        private async Task RemoveExistsUser()
        {
            Console.WriteLine("Remove exists user");
            var removeUserRequest = @"{""RemoveUser"":{""Id"":999}}";

            var content = new StringContent(removeUserRequest, Encoding.UTF8, "application/json");
            await _httpClient.PostAsync("/Auth/RemoveUser", content);
            Console.WriteLine();
        }

        private async Task RemoveNonExistUser()
        {
            Console.WriteLine("Remove non exist user");
            var removeUserRequest = @"{""RemoveUser"":{""Id"":1000}}";

            var content = new StringContent(removeUserRequest, Encoding.UTF8, "application/json");
            await _httpClient.PostAsync("/Auth/RemoveUser", content);
            Console.WriteLine();
        }
    }
}
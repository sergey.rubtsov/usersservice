using System;
using System.Collections.Specialized;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace UsersService.ConsoleApp
{
    internal class GetUserInfoDemonstrator : DemonstratorBase
    {
        private readonly HttpClient _httpClient;

        public GetUserInfoDemonstrator(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public override string MethodName { get; } = "UserInfo";

        protected override async Task ExecuteCases()
        {
            await GetUserInfo();
        }

        private async Task GetUserInfo()
        {
            Console.WriteLine("Get exists UserInfo as HTML");

            var query = BuildQuery(("id", 999.ToString()));
            await _httpClient.GetAsync($"Public/UserInfo?{query}");
        }

        private string? BuildQuery(params (string key, string value)[] keyValuePairs)
        {
            NameValueCollection query = HttpUtility.ParseQueryString(string.Empty);
            foreach (var (key, value) in keyValuePairs)
                query.Add(key, value);
            return query.ToString();
        }
    }
}
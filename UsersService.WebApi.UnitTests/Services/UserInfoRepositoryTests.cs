using System.Threading;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using UsersService.Domain;
using UsersService.WebApi.Exceptions;
using UsersService.WebApi.Models;
using UsersService.WebApi.Services;

namespace UsersService.WebApi.UnitTests.Services
{
    [TestFixture]
    public class UserInfoRepositoryTests
    {
        private Mock<IUserInfoCache> _userInfoCacheMock;
        private Mock<IUsersServiceContext> _usersServiceContextMock;

        [SetUp]
        public void SetUp()
        {
            _userInfoCacheMock = new Mock<IUserInfoCache>();
            _usersServiceContextMock = new Mock<IUsersServiceContext>();
        }

        private UserInfoRepository CreateRepository()
        {
            return new UserInfoRepository(_usersServiceContextMock.Object, _userInfoCacheMock.Object);
        }

        [Test]
        public async Task AddNew_AddsToDbContextNewUserInfo()
        {
            var userInfo = new UserInfo();

            await CreateRepository().AddNew(userInfo);

            _usersServiceContextMock.VerifyOnce(context => context.AddAsync(userInfo, It.IsAny<CancellationToken>()));
            _usersServiceContextMock.VerifyOnce(context => context.SaveChangesAsync(It.IsAny<CancellationToken>()));
        }

        [Test]
        public async Task AddNew_AddsToCacheNewUserInfo()
        {
            var userInfo = new UserInfo();

            await CreateRepository().AddNew(userInfo);

            _userInfoCacheMock.VerifyOnce(cache => cache.AddNew(userInfo));
        }

        [Test]
        public void AddNew_ThrowsUserAlreadyExistsExceptionWhenCacheIsExistReturnTrue()
        {
            var userInfo = new UserInfo { Id = 23 };
            _userInfoCacheMock.Setup(cache => cache.IsExist(userInfo.Id)).Returns(true);

            var userAlreadyExistsException = Assert.ThrowsAsync<UserAlreadyExistsException>(async () => await CreateRepository().AddNew(userInfo));

            Assert.AreEqual(ErrorId.UserAlreadyExists, userAlreadyExistsException.ErrorId);
            Assert.AreEqual($"User with id {userInfo.Id} already exist", userAlreadyExistsException.Message);
        }

        [Test]
        public async Task Remove_UpdateUserInfoWithStatusDeleted()
        {
            var userInfo = new UserInfo { Id = 1, Status = Status.Active };
            _userInfoCacheMock
                .Setup(cache => cache.Get(It.IsAny<int>()))
                .Returns(userInfo);

            await CreateRepository().Remove(userInfo.Id);

            _usersServiceContextMock.VerifyOnce(context => context.Update(It.Is<UserInfo>(e => e.Status == Status.Deleted)));
            _usersServiceContextMock.VerifyOnce(context => context.SaveChangesAsync(It.IsAny<CancellationToken>()));
        }

        [TestCase(Status.Active)]
        [TestCase(Status.Blocked)]
        [TestCase(Status.Deleted)]
        public async Task SetStatus_UpdateUserInfoWithStatus(Status expectedStatus)
        {
            var userInfo = new UserInfo { Id = 1, Status = Status.New };
            _userInfoCacheMock
                .Setup(cache => cache.Get(It.IsAny<int>()))
                .Returns(userInfo);

            await CreateRepository().SetStatus(userInfo.Id, expectedStatus);

            _usersServiceContextMock.VerifyOnce(context => context.Update(It.Is<UserInfo>(e => e.Status == expectedStatus)));
            _usersServiceContextMock.VerifyOnce(context => context.SaveChangesAsync(It.IsAny<CancellationToken>()));
        }
    }
}
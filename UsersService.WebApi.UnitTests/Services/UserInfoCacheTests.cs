using NUnit.Framework;
using UsersService.Domain;
using UsersService.WebApi.Services;

namespace UsersService.WebApi.UnitTests.Services
{
    [TestFixture]
    public class UserInfoCacheTests
    {
        private UserInfoCache CreateCache()
        {
            return new UserInfoCache();
        }

        [Test]
        public void IsExist_ReturnsFalseWhenCacheIsEmpty()
        {
            var cache = CreateCache();
            var userInfoId = 21;

            var isExist = cache.IsExist(userInfoId);

            Assert.IsFalse(isExist);
        }

        [Test]
        public void IsExist_ReturnsTrueAfterAdded()
        {
            var cache = CreateCache();
            var userInfo = new UserInfo { Id = 45 };

            cache.AddNew(userInfo);

            var isExist = cache.IsExist(userInfo.Id);

            Assert.IsTrue(isExist);
        }

        [Test]
        public void Reset_FillCacheWithNewList()
        {
            var cache = CreateCache();
            var initialList = new[] { new UserInfo { Id = 1 }, new UserInfo { Id = 2 } };
            cache.Reset(initialList);

            Assert.IsNotNull(cache.Get(1));
            Assert.IsNotNull(cache.Get(2));
        }

        [Test]
        public void Reset_RewriteAllCacheWithNewList()
        {
            var cache = CreateCache();
            var initialList = new[] { new UserInfo { Id = 1 }, new UserInfo { Id = 2 } };
            cache.Reset(initialList);

            var newList = new[] { new UserInfo { Id = 2 }, new UserInfo { Id = 3 } };
            cache.Reset(newList);

            Assert.IsNull(cache.Get(1));
            Assert.IsNotNull(cache.Get(2));
            Assert.IsNotNull(cache.Get(3));
        }
    }
}
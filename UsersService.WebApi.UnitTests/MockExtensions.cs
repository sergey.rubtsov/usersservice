using System;
using System.Linq.Expressions;
using Moq;

namespace UsersService.WebApi.UnitTests
{
    public static class MockExtensions
    {
        public static void VerifyOnce<T, TResult>(this Mock<T> mock, Expression<Func<T, TResult>> expression)
            where T : class
        {
            mock.Verify(expression, Times.Once);
        }

        public static void VerifyOnce<T>(this Mock<T> mock, Expression<Action<T>> expression)
            where T : class
        {
            mock.Verify(expression, Times.Once);
        }
    }
}
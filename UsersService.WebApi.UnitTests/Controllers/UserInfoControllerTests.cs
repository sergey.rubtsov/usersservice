using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using UsersService.Domain;
using UsersService.WebApi.Controllers;
using UsersService.WebApi.Exceptions;
using UsersService.WebApi.Services;

namespace UsersService.WebApi.UnitTests.Controllers
{
    [TestFixture]
    public class UserInfoControllerTests
    {
        private Mock<IUserInfoRepository> _userInfoRepositoryMock;

        [SetUp]
        public void SetUp()
        {
            _userInfoRepositoryMock = new Mock<IUserInfoRepository>();
            _userInfoRepositoryMock
                .Setup(repository => repository.SetStatus(It.IsAny<int>(), It.IsAny<Status>()))
                .ReturnsAsync(new UserInfo());
        }

        private UserInfoController CreateController()
        {
            return new UserInfoController(_userInfoRepositoryMock.Object);
        }

        private static void VerifyFailResponse(ErrorId expectedErrorId, string expectedMessage, Response actual)
        {
            Assert.IsInstanceOf<FailResponse>(actual);
            var failResponse = (FailResponse) actual;
            Assert.IsFalse(failResponse.Success, nameof(FailResponse.Success));
            Assert.AreEqual(expectedMessage, failResponse.Message, nameof(FailResponse.Message));
            Assert.AreEqual(expectedErrorId, failResponse.ErrorId, nameof(FailResponse.ErrorId));
        }

        private static void VerifyResponse(UserInfo expectedUserInfo, SetStatusResponse actualResponse)
        {
            Assert.AreEqual(expectedUserInfo.Id, actualResponse.UserId, nameof(UserInfo.Id));
            Assert.AreEqual(expectedUserInfo.Status, actualResponse.Status, nameof(UserInfo.Status));
            Assert.AreEqual(expectedUserInfo.Name, actualResponse.Name, nameof(UserInfo.Name));
        }

        private void SetupRepositorySetStatusReturn(UserInfo userInfo)
        {
            _userInfoRepositoryMock
                .Setup(repository => repository.SetStatus(It.IsAny<int>(), It.IsAny<Status>()))
                .ReturnsAsync(userInfo);
        }

        [Test]
        public async Task CreateUser_ReturnsUserInfoInResponseAfterCreateInRepository()
        {
            var userInfo = new UserInfo();
            var controller = CreateController();

            var actual = await controller.CreateUser(new UserInfoCreateRequest { UserInfo = userInfo });

            _userInfoRepositoryMock.VerifyOnce(repository => repository.AddNew(userInfo));
            Assert.IsInstanceOf<UserInfoCreateResponse>(actual);
            var userInfoCreateResponse = (UserInfoCreateResponse) actual;
            Assert.AreSame(userInfo, userInfoCreateResponse.UserInfo);
            Assert.IsTrue(userInfoCreateResponse.Success);
        }

        [Test]
        public async Task CreateUser_ReturnsFailResponseAfterUsersServiceExceptionInRepository()
        {
            const ErrorId errorId = ErrorId.UserAlreadyExists;
            const string exceptionMessage = "exception message";
            _userInfoRepositoryMock
                .Setup(repository => repository.AddNew(It.IsAny<UserInfo>()))
                .ThrowsAsync(new UsersServiceException(errorId, exceptionMessage));

            var actual = await CreateController().CreateUser(new UserInfoCreateRequest { UserInfo = new UserInfo() });

            VerifyFailResponse(errorId, exceptionMessage, actual);
        }

        [Test]
        public async Task CreateUser_ReturnsFailResponseAfterAnyExceptionInRepository()
        {
            const string exceptionMessage = "exception message";
            _userInfoRepositoryMock
                .Setup(repository => repository.AddNew(It.IsAny<UserInfo>()))
                .ThrowsAsync(new Exception(exceptionMessage));

            var actual = await CreateController().CreateUser(new UserInfoCreateRequest { UserInfo = new UserInfo() });

            VerifyFailResponse(ErrorId.Unknown, exceptionMessage, actual);
        }

        [Test]
        public async Task RemoveUser_ReturnsUserInfoInResponseAfterRemoveInRepository()
        {
            const int userInfoId = 12;
            var userInfo = new UserInfo { Id = userInfoId };
            var controller = CreateController();

            var actual = await controller.RemoveUser(new UserInfoRemoveRequest { RemoveUser = userInfo });

            _userInfoRepositoryMock.VerifyOnce(repository => repository.Remove(userInfoId));
            Assert.IsInstanceOf<UserInfoRemoveResponse>(actual);
            var response = (UserInfoRemoveResponse) actual;
            Assert.AreEqual("User was removed", response.Message);
            Assert.IsTrue(response.Success);
        }

        [Test]
        public async Task RemoveUser_ReturnsUserInfoInResponseFromRepository()
        {
            var userInfoFromRepository = new UserInfo();
            _userInfoRepositoryMock
                .Setup(repository => repository.Remove(It.IsAny<int>()))
                .ReturnsAsync(userInfoFromRepository);

            var controller = CreateController();

            var actual = await controller.RemoveUser(new UserInfoRemoveRequest { RemoveUser = new UserInfo() });

            Assert.IsInstanceOf<UserInfoRemoveResponse>(actual);
            var response = (UserInfoRemoveResponse) actual;
            Assert.AreSame(userInfoFromRepository, response.UserInfo);
        }

        [Test]
        public async Task RemoveUser_ReturnsFailResponseAfterUsersServiceExceptionInRepository()
        {
            const ErrorId errorId = ErrorId.NotFound;
            const string exceptionMessage = "exception message";
            _userInfoRepositoryMock
                .Setup(repository => repository.Remove(It.IsAny<int>()))
                .ThrowsAsync(new UsersServiceException(errorId, exceptionMessage));

            var actual = await CreateController().RemoveUser(new UserInfoRemoveRequest { RemoveUser = new UserInfo() });

            VerifyFailResponse(errorId, exceptionMessage, actual);
        }

        [Test]
        public async Task RemoveUser_ReturnsFailResponseAfterAnyExceptionInRepository()
        {
            const string exceptionMessage = "exception message";
            _userInfoRepositoryMock
                .Setup(repository => repository.Remove(It.IsAny<int>()))
                .ThrowsAsync(new Exception(exceptionMessage));

            var actual = await CreateController().RemoveUser(new UserInfoRemoveRequest { RemoveUser = new UserInfo() });

            VerifyFailResponse(ErrorId.Unknown, exceptionMessage, actual);
        }

        [Test]
        public async Task SetStatus_CallsRepositorySetStatus()
        {
            const int userInfoId = 12;
            const Status newStatus = Status.Active;
            var controller = CreateController();

            await controller.SetStatus(new SetStatusRequest { Id = userInfoId, NewStatus = newStatus.ToString() });

            _userInfoRepositoryMock.VerifyOnce(repository => repository.SetStatus(userInfoId, newStatus));
        }

        [Test]
        public async Task SetStatus_ReturnsUserInfoInResponseFromRepository()
        {
            var userInfo = new UserInfo { Id = 12, Name = "name", Status = Status.Active };
            SetupRepositorySetStatusReturn(userInfo);
            var controller = CreateController();

            var response = await controller.SetStatus(new SetStatusRequest { Id = userInfo.Id, NewStatus = userInfo.Status.ToString() });

            Assert.IsInstanceOf<SetStatusResponse>(response);
            var setStatusResponse = (SetStatusResponse) response;
            VerifyResponse(userInfo, setStatusResponse);
        }

        [Test]
        public async Task SetStatus_ReturnsFailResponseAfterUsersServiceExceptionInRepository()
        {
            const ErrorId errorId = ErrorId.NotFound;
            const string exceptionMessage = "exception message";
            const Status anyStatus = Status.Blocked;
            _userInfoRepositoryMock
                .Setup(repository => repository.SetStatus(It.IsAny<int>(), It.IsAny<Status>()))
                .ThrowsAsync(new UsersServiceException(errorId, exceptionMessage));

            var actual = await CreateController().SetStatus(new SetStatusRequest { NewStatus = anyStatus.ToString() });

            VerifyFailResponse(errorId, exceptionMessage, actual);
        }

        [Test]
        public async Task SetStatus_ReturnsFailResponseAfterAnyExceptionInRepository()
        {
            const string exceptionMessage = "exception message";
            const Status anyStatus = Status.Blocked;
            _userInfoRepositoryMock
                .Setup(repository => repository.SetStatus(It.IsAny<int>(), It.IsAny<Status>()))
                .ThrowsAsync(new Exception(exceptionMessage));

            var actual = await CreateController().SetStatus(new SetStatusRequest { NewStatus = anyStatus.ToString() });

            VerifyFailResponse(ErrorId.Unknown, exceptionMessage, actual);
        }

        [Test]
        public async Task GetUserInfo_CallsRepositoryGet()
        {
            const int userInfoId = 12;

            await CreateController().GetUserInfo(userInfoId);

            _userInfoRepositoryMock.VerifyOnce(repository => repository.Get(userInfoId));
        }

        [Test]
        public async Task GetUserInfo_ReturnsViewResultWithModel()
        {
            var userInfo = new UserInfo();
            _userInfoRepositoryMock
                .Setup(repository => repository.Get(It.IsAny<int>()))
                .ReturnsAsync(userInfo);

            var actionResult = await CreateController().GetUserInfo(userInfo.Id);

            Assert.IsInstanceOf<ViewResult>(actionResult);
            var viewResult = (ViewResult) actionResult;
            Assert.AreSame(userInfo, viewResult.Model);
            StringAssert.Contains("UserInfo", viewResult.ViewName);
        }

        [Test]
        public async Task GetUserInfo_ReturnsNotFoundWhenRepositoryThrowUserNotFoundException()
        {
            var userInfo = new UserInfo();
            _userInfoRepositoryMock
                .Setup(repository => repository.Get(It.IsAny<int>()))
                .ThrowsAsync(new UserNotFoundException("any message"));

            var viewResult = await CreateController().GetUserInfo(userInfo.Id);

            Assert.IsInstanceOf<NotFoundResult>(viewResult);
        }
    }
}
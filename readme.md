### Test project

ASP.NET Core service and console application to call methods on service.

#### Database

Database creates with model on application startup if it doesn't exist.

Connection string stores in `UsersService.WebApi/appsettings.Development.json`. Current value: `Server=localhost;Port=3306;Database=usersServiceDb;User=root;Password=password;`

#### IISExpress settings

Stores in `UsersService.WebApi/Properties/launchSettings.json`. By default service starts on `https://localhost:44329`

#### Basic authorization credentials

Stores in `UsersService.WebApi/appsettings.Development.json`. Current values:
username: `user`, password: `password`.

#### Console application

Base address, username and password are hardcoded in class `UsersService.ConsoleApp.HttpClienFactory`